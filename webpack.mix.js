let mix = require('laravel-mix');

mix.setPublicPath('./')

mix.sass('src/scss/app.scss', 'assets/')
    .options({
        processCssUrls: true
    });

mix.js('src/js/app.js', 'assets/')
    .webpackConfig({
        module: {
            rules: [
                {
                    test: /\.jsx?$/,
                    exclude: /node_modules(?!\/foundation-sites)|bower_components/,
                    use: [
                        {
                            loader: 'babel-loader',
                            options: Config.babel()
                        }
                    ]
                }
            ]
        }
    })
    .sourceMaps()
    .version();

mix.copy('src/images', 'images')
    .version('images');