<?php
function getVersion($resourcePath)
{
    // load the mix manifest file
    $str = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/mix-manifest.json');
    $json = json_decode($str, true);
    $resource = $json[$resourcePath];

    echo $resource;
}
?>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:700|Open+Sans:400,700" rel="stylesheet">
<link rel="stylesheet" href="<?php getVersion('/assets/app.css'); ?>">
