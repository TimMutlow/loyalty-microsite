window.$ = window.jQuery = require('jquery');
require('what-input');
require('foundation-sites');
require('bootstrap');
var Shuffle = require('shufflejs');

$(document).foundation();

// Shuffle config
var element = document.querySelector('.magasins-page .magasins .grid-x:last-of-type');
if( element ) {
  var shuffleInstance = new Shuffle(element, {
      itemSelector: '.magasins-page .magasins .grid-x:last-of-type .cell',
      delimeter: ','
  });
}

// Post-jQuery Set up
jQuery(function () {

    // Setup change listeners for the recette filters
    if (jQuery('.recettes-page')) {
        jQuery('#recipe').on('click', function () {
            if (this.checked) {
                jQuery('div[data-recette]').fadeIn();
            } else {
                jQuery('div[data-recette]').fadeOut();
            }
        });

        jQuery('#video').on('click', function () {
            if (this.checked) {
                jQuery('div[data-video]').fadeIn();
            } else {
                jQuery('div[data-video]').fadeOut();
            }
        });

        jQuery('#advice').on('click', function () {
            if (this.checked) {
                jQuery('div[data-conseil]').fadeIn();
            } else {
                jQuery('div[data-conseil]').fadeOut();
            }
        });
    }

    // magasins
    // Setup event handlers for the magasin filters
    // Advanced filtering
    jQuery('#search-text').keyup ( function (evt) {
        var searchText = $(this).val().toLowerCase();

        shuffleInstance.filter(function (element, shuffle) {
            var titleElement = element.querySelector('li strong');
            var titleText = titleElement.textContent.toLowerCase().trim();
            return titleText.indexOf(searchText) !== -1;
        });
    });

    jQuery('#recherche a').click( function (evt) {
      filterValue = jQuery(this).text();
      if(filterValue.length > 1) {
        shuffleInstance.filter();
      } else {
        shuffleInstance.filter(filterValue);
      }
    });

    // If it is the magasins-page get the fab button and listen for scroll event to show it
    var fab = jQuery('.fab');
    if (fab.length > 0) {
        window.addEventListener('scroll', function (e) {
            if (window.scrollY >= window.innerHeight) {
                fab.removeClass('invisible');
            } else {
                fab.addClass('invisible');
            }
        });
    }

    // Setup the Owl Carousel
    var owlOptions = {
        stagePadding: 35,
        loop: false,
        margin: 30,
        nav: false,
        dots: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            1000: {
                items: 3
            }
        }
    };

    var owls = jQuery(".owl-carousel");

    if (owls.length > 0) {
        owls.owlCarousel(owlOptions);

        jQuery('.next-button').click(function () {
            jQuery(owls).trigger('next.owl.carousel');
        });

        jQuery('.prev-button').click(function () {
            jQuery(owls).trigger('prev.owl.carousel');
        });
    }

    // Equalise the heights of adjacent faq entries
    var equalisers = jQuery('.faqs .revealing-panel h3 .grid-x');

    if (equalisers.length > 0) {

        var counter = 0;    // Count the number of entries processed (these are done in pairs)
        var maxHeight = 0;  //
        var previousEntry = undefined;

        jQuery.each(equalisers, function () {
            maxHeight = Math.max(jQuery(this).height(), maxHeight);
            counter++;

            if (counter == 2) {

                jQuery(this).height(maxHeight);
                previousEntry.height(maxHeight);

                counter = 0;
                maxHeight = 0;
            } else {
                previousEntry = jQuery(this);
            }
        });

    }

    // Add revealing sections behaviour (individual accordions)
    jQuery('.revealing-panel h3').on('click', function () {

        if (jQuery(this).hasClass('active')) {
            jQuery('.revealing-panel div.collapse').slideUp();
            jQuery('.revealing-panel h3').removeClass('active');
        } else {
            jQuery('.revealing-panel div.collapse').slideUp();
            jQuery('.revealing-panel h3').removeClass('active');
            jQuery(this).addClass('active');
            jQuery(this).siblings('div').slideDown();
        }
    });
});
